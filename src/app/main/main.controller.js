(function () {
  'use strict';

  angular
    .module('testAngularSetup')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($scope) {
    $scope.awesomeThings = ' Heyy you got your page';
  }
})();
