(function() {
  'use strict';

  angular
    .module('testAngularSetup')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
